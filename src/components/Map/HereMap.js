import React, {useContext, useEffect, useState} from 'react';
import PointStartIcon from '../../assets/icons/ic_a.svg';
import PointEndIcon from '../../assets/icons/ic_b.svg';
import PointClientIcon from '../../assets/icons/ic_user.svg';
import PointDriverIcon from '../../assets/icons/ic_car.svg';
import './style.scss';
import {DataContext} from '../../store/context';

let clientMarker = null;
let driverMarker = null;
let hMap = null;

export const HereMap = () => {
  const {map} = useContext(DataContext);

  const urlParams = new URLSearchParams(window.location.search);
  const order = urlParams.get('orderId');

  const url = `${process.env.REACT_APP_API}_${order}`;

  const mapRef = React.useRef(null);
  const socket = React.useRef(new WebSocket(url));

  const [routingParameters, setRoutingParameters] = useState({
    routingMode: 'fast',
    transportMode: 'car',
    return: 'polyline',
    origin: '0,0',
    destination: '0,0',
  });

  useEffect(() => {
    const {latitude, longitude, toCoordinates} = map;

    const destinationLat = toCoordinates[toCoordinates.length - 1].latitude;
    const destinationLng = toCoordinates[toCoordinates.length - 1].longitude;

    setRoutingParameters(state => ({
      ...state,
      origin: `${latitude},${longitude}`,
      destination: `${destinationLat},${destinationLng}`,
    }));
  }, [map]);

  const [position, setPosition] = useState({
    LocationFromDriverNotification: {
      Latitude: 0,
      Longitude: 0,
      OrderId: 0,
    },
    LocationFromClientNotification: {
      Latitude: 0,
      Longitude: 0,
      OrderId: 0,
    },
  });

  useEffect(() => {
    socket.current.onmessage = ({data}) => {
      const {NotificationName, Notification} = JSON.parse(data);
      setPosition(state => ({...state, [NotificationName]: Notification}));
    };
  });

  useEffect(() => () => socket.current.close(), [socket]);

  React.useLayoutEffect(() => {
    if (!mapRef.current) return;

    const {H} = window;
    const pointStart = new H.map.Icon(PointStartIcon);
    const pointEnd = new H.map.Icon(PointEndIcon);
    const pointClient = new H.map.Icon(PointClientIcon);
    const pointDriver = new H.map.Icon(PointDriverIcon);

    const platform = new H.service.Platform({
      apikey: process.env.REACT_APP_MAP_KEY,
    });

    const defaultLayers = platform.createDefaultLayers({lg: 'ru'});

    hMap = new H.Map(mapRef.current, defaultLayers.vector.normal.map, {
      center: {
        lat: map.latitude,
        lng: map.longitude,
      },
      zoom: 14,
      pixelRatio: window.devicePixelRatio || 1,
    });

    const onResult = function (result) {
      if (result.routes.length) {
        result.routes[0].sections.forEach(section => {
          let linestring = H.geo.LineString.fromFlexiblePolyline(section.polyline);

          let routeLine = new H.map.Polyline(linestring, {
            style: {strokeColor: '#58B683', lineWidth: 5},
          });

          let startMarker = new H.map.Marker(section.departure.place.location, {icon: pointStart});

          let endMarker = new H.map.Marker(section.arrival.place.location, {icon: pointEnd});

          hMap.addObjects([routeLine, startMarker, endMarker]);
          hMap.getViewModel().setLookAtData({bounds: routeLine.getBoundingBox()});
        });
      }
    };

    const router = platform.getRoutingService(null, 8);
    router.calculateRoute(routingParameters, onResult);

    driverMarker = new H.map.Marker({lat: 0, lng: 0}, {icon: pointDriver});

    clientMarker = new H.map.Marker({lat: 0, lng: 0}, {icon: pointClient});

    hMap.addObjects([clientMarker, driverMarker]);

    // eslint-disable-next-line
    const behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(hMap));
    // eslint-disable-next-line
    const ui = H.ui.UI.createDefault(hMap, defaultLayers, 'ru-RU');

    // add a resize listener to make sure that the map occupies the whole container
    window.addEventListener('resize', () => hMap.getViewPort().resize());

    return () => hMap.dispose();
  }, [mapRef, routingParameters, map]);

  React.useEffect(() => {
    if (clientMarker && driverMarker) {
      clientMarker.setGeometry({
        lat: position.LocationFromClientNotification.Latitude,
        lng: position.LocationFromClientNotification.Longitude,
      });
      driverMarker.setGeometry({
        lat: position.LocationFromDriverNotification.Latitude,
        lng: position.LocationFromDriverNotification.Longitude,
      });
    }
  }, [position]);

  // React.useEffect(() => console.log(position), [position]);

  return <div className="map" ref={mapRef} />;
};
