import React from 'react';
import './style.scss';

export const Preloader = () => <div className="lds-dual-ring" />;
