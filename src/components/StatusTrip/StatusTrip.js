import React, {useContext} from 'react';
import {DataContext} from '../../store/context';
import './style.scss';

export const StatusTrip = () => {
  const {
    map: {status},
  } = useContext(DataContext);

  const getStatusTrip = status => {
    switch (status) {
      case 0:
        return 'Статус активен';
      case 1:
        return 'В поиске';
      case 2:
        return 'В пути';
      case 3:
        return 'Поездка завершена';
      case 4:
        return 'Поездка отменена водителем';
      case 5:
        return 'Поездка отменена клиентом';
      case 6:
        return 'Ожидаем клиента';
      default:
        return 'Статус поездки не доступен';
    }
  };

  return <div className="statusTrip">{getStatusTrip(status)}</div>;
};
