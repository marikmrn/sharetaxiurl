import React, {useContext} from 'react';
import {Preloader} from '../../Preloader/Preloader';
import {DataContext} from '../../../store/context';
import {useAvatar} from '../../../utils/useAvatar';
import EmptyIcon from '../../../assets/icons/empty-avatar.png';
import {ReactComponent as CarIcon} from '../../../assets/icons/carIcon.svg';
import {ReactComponent as LoupeIcon} from '../../../assets/icons/loupe.svg';
import '../style.scss';

export const DriverProfile = () => {
  const {
    map: {price},
    driver: {
      firstName,
      lastName,
      driverPhotoIdentifier,
      carInfo: {brand, model, year, number, color},
    },
  } = useContext(DataContext);

  const {avatar, loaderAvatar} = useAvatar(driverPhotoIdentifier);

  return (
    <div className="profile-driver">
      <span className="profile__label">Водитель</span>
      <div className="profile">
        <div className="profile__card">
          <a
            href={avatar ? avatar : null}
            target="_blank"
            rel="noreferrer"
            className="profile__avatar profile__avatar-loupe"
          >
            {loaderAvatar ? <Preloader /> : <img src={avatar ? avatar : EmptyIcon} alt="avatar" />}
            <LoupeIcon />
          </a>
          <div className="profile__info">
            <ul>
              <li className="profile__info_title">
                {lastName} {firstName}
              </li>
              {/*<li>*/}
              {/*  <a href={`tel:${phoneNumber}`} className="profile__info_phone">*/}
              {/*    +{divisionOfNumber(phoneNumber)}*/}
              {/*  </a>*/}
              {/*</li>*/}
            </ul>
            <div className="profile__info_price">{price}₴</div>
          </div>
        </div>
        <div className="profile__card profile__card-auto ">
          <span className="profile__avatar">
            <CarIcon />
          </span>
          <div className="profile__info">
            <ul>
              <li className="profile__info_title">
                {brand} {model} {year}г.
              </li>
              <li className="profile__info_numberCar">{number}</li>
              <li className="profile__info_color">{color}</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
