import React from 'react';
import {DriverProfile} from './Driver/Driver';
import {UserProfile} from './User/User';
import {StatusTrip} from '../StatusTrip/StatusTrip';
import './style.scss';

export const Profiles = () => (
  <div className="profiles">
    <StatusTrip />
    <DriverProfile />
    <UserProfile />
  </div>
);
