import React, {useContext} from 'react';
import {Preloader} from '../../Preloader/Preloader';
import {DataContext} from '../../../store/context';
import {useAvatar} from '../../../utils/useAvatar';
import EmptyIcon from '../../../assets/icons/empty-avatar.png';
import {ReactComponent as LoupeIcon} from '../../../assets/icons/loupe.svg';
import '../style.scss';

export const UserProfile = () => {
  const {
    user: {firstName, lastName, mainPhotoIdentifier},
  } = useContext(DataContext);

  const {avatar, loaderAvatar} = useAvatar(mainPhotoIdentifier);

  return (
    <div classNwame="profile-client">
      <span className="profile__label">Клиент</span>
      <div className="profile">
        <div className="profile__card">
          <a
            href={avatar ? avatar : null}
            target="_blank"
            rel="noreferrer"
            className="profile__avatar"
          >
            {loaderAvatar ? <Preloader /> : <img src={avatar ? avatar : EmptyIcon} alt="avatar" />}
            <LoupeIcon />
          </a>
          <div className="profile__info">
            <ul>
              <li className="profile__info_title">
                {lastName} {firstName}
              </li>
              {/*<li>*/}
              {/*  <a href={`tel:${phoneNumber}`} className="profile__info_phone">*/}
              {/*    +{divisionOfNumber(phoneNumber)}*/}
              {/*  </a>*/}
              {/*</li>*/}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};
