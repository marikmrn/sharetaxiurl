import React, {createContext, useReducer} from 'react';

const TYPES = {
  SET_MAP_INFO: 'SET_MAP_INFO',
  SET_DRIVER_INFO: 'SET_DRIVER_INFO',
  SET_USER_INFO: 'SET_USER_INFO',
  LOADING: 'LOADING',
};

const noop = () => {};

export const DataContext = createContext({
  map: {},
  user: {},
  driver: {},
  loading: false,
  getData: noop(),
});

const initialSate = {
  map: {
    id: 0,
    status: 0,
    latitude: '',
    longitude: '',
    toCoordinates: [
      {
        latitude: '',
        longitude: '',
      },
    ],
    price: 0,
  },
  user: {
    firstName: '',
    lastName: '',
    phoneNumber: '',
  },
  driver: {
    firstName: '',
    lastName: '',
    phoneNumber: '',
    driverPhotoIdentifier: '',
    carInfo: {
      brand: '',
      model: '',
      year: '',
      number: '',
      color: '',
    },
  },
  loading: false,
};

const reducer = (state = initialSate, {type, payload}) => {
  switch (type) {
    case TYPES.SET_MAP_INFO:
      return {
        ...state,
        map: {...payload},
      };
    case TYPES.SET_DRIVER_INFO:
      return {
        ...state,
        driver: {...payload},
      };
    case TYPES.SET_CAR_INFO:
      return {
        ...state,
        carInfo: {...payload},
      };
    case TYPES.SET_USER_INFO:
      return {
        ...state,
        user: {...payload},
      };
    case TYPES.LOADING:
      return {
        ...state,
        loading: payload,
      };

    default:
      return state;
  }
};

export const DataProvider = ({children}) => {
  const [state, dispatch] = useReducer(reducer, initialSate);
  const {map, driver, user, loading} = state;
  const getData = async id => {
    try {
      dispatch({type: TYPES.LOADING, payload: true});
      await fetch(`${process.env.REACT_APP_API_SHARING}${id}`)
        .then(resp => resp.json())
        .then(({sharingOrderInfo, sharingDriverInfo, sharingClientInfo}) => {
          dispatch({type: TYPES.SET_MAP_INFO, payload: sharingOrderInfo});
          dispatch({type: TYPES.SET_DRIVER_INFO, payload: sharingDriverInfo});
          dispatch({type: TYPES.SET_USER_INFO, payload: sharingClientInfo});
          dispatch({type: TYPES.LOADING, payload: false});
        });
    } catch (err) {
      dispatch({type: TYPES.LOADING, payload: false});
    }
  };

  return (
    <DataContext.Provider value={{getData, map, driver, user, loading}}>
      {children}
    </DataContext.Provider>
  );
};
