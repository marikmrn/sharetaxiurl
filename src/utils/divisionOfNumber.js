export const divisionOfNumber = number => {
  return `${number}`.replace(/(\d{2})(\d{3})(\d{3})(\d{2})(\d{2})/g, '$1 $2 $3 $4 $5');
};
