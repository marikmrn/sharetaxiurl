import {useEffect, useState} from 'react';

export const useAvatar = avatarId => {
  const [avatar, setAvatar] = useState('');
  const [loaderAvatar, setAvatarLoader] = useState(false);
  useEffect(() => {
    if (avatarId) {
      try {
        setAvatarLoader(true);
        fetch(`${process.env.REACT_APP_API_FILE}${avatarId}`)
          .then(resp => resp.blob())
          .then(blob => URL.createObjectURL(blob))
          .then(img => {
            setAvatar(img);
            setAvatarLoader(false);
          });
      } catch (err) {
        setAvatarLoader(false);
        console.error(err);
      }
    }
  }, [avatarId]);

  return {avatar, loaderAvatar};
};
