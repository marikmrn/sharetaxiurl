import React from 'react';
import {render} from 'react-dom';
import {App} from './App';
import './index.scss';
import {DataProvider} from './store/context';

render(
  <DataProvider>
    <App />
  </DataProvider>,
  document.getElementById('root')
);
