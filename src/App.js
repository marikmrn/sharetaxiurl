import React, {useContext, useEffect} from 'react';
import {DataContext} from './store/context';
import {HereMap} from './components/Map/HereMap';
import {Profiles} from './components/Profiles/Profiles';
import './index.scss';

export const App = () => {
  const urlParams = new URLSearchParams(window.location.search);
  const id = urlParams.get('sharingId');

  const {getData} = useContext(DataContext);

  useEffect(() => getData(id), [id]);

  return (
    <div className="wrapper">
      <HereMap />
      <Profiles />
    </div>
  );
};
