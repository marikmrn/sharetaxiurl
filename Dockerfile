FROM node:14.15.4-alpine as builder
RUN mkdir -p /app
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json  /app
RUN yarn install

COPY . /app
RUN yarn build

FROM nginx:1.19.5-alpine
COPY --from=builder /app/build /usr/share/nginx/html
